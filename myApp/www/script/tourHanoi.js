var tabColor = ["purple", "blue", "green","yellow","red"]
var currentColumn = null

function Disque () {
    var disque = {
        constructor(parent, columnIndex) {
            disque._parentColumn = parent
            disque._columnIndex = columnIndex
            disque._isSelected = false
            disque._width = 0
    
            disque._disqueContainer = document.createElement('div')
            disque._disqueContainer.style.height = "50px"
            disque._disqueContainer.style.margin = "0px auto"
            disque._parentColumn.appendChild(disque._disqueContainer)
        },
    
        setBackgroundColor(color){
            disque._disqueContainer.style.backgroundColor = color
            return disque
        },
    
        setWidth(width){
            disque._width = width
            disque._disqueContainer.style.width = width.toString() + "%"
            return disque
        },
        getWith(){ return disque._width},
    
        getColumnIndex(){ return disque._columnIndex},
    
        changeParent(newColumn, columnIndex){
            this._parentColumn.removeChild(disque._disqueContainer)
            this._parentColumn = newColumn
            this._columnIndex = columnIndex
            this._parentColumn.appendChild(disque._disqueContainer)
        },
        selected(){
            this._isSelected = !disque._isSelected
            this._disqueContainer.style.border = disque._isSelected ? "5px solid black" : ""
        }
    }
    return disque
}


function Column(){
    var column = {
        _parent: null,
        _index: 0,
        _disques: [],
        _columnContainer: null,
        constructor: function(parent, index) {
            column._parent = parent
            column._index = index
            column._disques = []
    
            column._columnContainer = document.createElement('div')
            column._columnContainer.classList.add('columnContainer')
            column._columnContainer.addEventListener('click', function() {
                column.selectColumn()
            })
    
    
            column._parent.appendChild(this._columnContainer)
        },
        createDisque: function(){
            var disque = Disque()
            disque.constructor(column._columnContainer)
            disque.setBackgroundColor(tabColor[column._disques.length])
            disque.setWidth(100 - 15* column._disques.length)
            column._disques.push(
                disque
            )
        },
        getDisques: function(){
            return column._disques
        },
    
        addDisque: function(disque){
            column._disques.push(disque)
        },
        removeDisque: function(index){
            column._disques.splice(index, 1)
        },
        selectColumn: function(){
            if(currentColumn == null)
                currentColumn = column;
        
            if(currentColumn._index === column._index){
                if(column._disques.length > 0){
                    var currentDisque = column._disques[column._disques.length - 1]
                    currentDisque.selected()
                    if(currentDisque._isSelected === false)
                        currentColumn = null;
                }else{
                    currentColumn = null;
                }
            }else{
                var currentDisque = currentColumn._disques[currentColumn._disques.length - 1]
                if(column._disques.length === 0 || column._disques[column._disques.length - 1].getWith() >currentDisque.getWith()){
                    column.addDisque(currentDisque)
                    currentColumn.removeDisque(currentColumn._disques.length - 1)
                    currentDisque.changeParent(column._columnContainer, column._index)
                }
                currentDisque.selected()
                currentColumn = null;
            }
        }
    }
    return column
}


var HanoiContainer = document.getElementById('TourDHanoi')
var columnList = []
for(var c = 0; c < 3; c ++){
    var col = Column();
    col.constructor(HanoiContainer, c)
    columnList.push(col)
}
var disquesCount = 3

for(var d = 0; d < disquesCount; d ++){
    columnList[0].createDisque()
}

/**
 * Cr�ation d'un �l�ment du sapin
 * @param parent { HTMLElement }
 * @param width { number }
 * @param color { string }
 */
function disqueCreator(parent,width,color,i) {
    //On cr�e l'element HTML
    
    
    
    var div = document.createElement("div")
    div.setAttribute("id", "d_" + i)
    // div.addEventListener("click", selectDisque)
    // on donne le style � cet element
    div.style.height = "25px"
    div.style.display = "flex"
    div.style.margin = "auto"
    div.style.backgroundColor = color
    div.style.width = (width * 100).toString() + "%"
    // on l'ajoute � son parent
    parent.appendChild(div)
    
    var disque = {
        "div": div,
        "colIndex": 0
    }
    tabHanoiGame[0].disques[i]= disque
    return disque
}

var tabHanoiGame = [
    {
        "name": "col1",
        "disques": new Array(5)
    },
    {
        "name": "col2",
        "disques": new Array(5)
    },
    {
        "name": "col3",
        "disques": new Array(5)
    },
]

var currentDisque = null
var currentColumn = null
/*
for (i = 0; i < 5; i++) {
    disqueCreator(document.getElementById("row"+i+"_col1"), 1-0.2*i, tabColor[i],i)
}
 */
// var tBody = document.getElementById("tBodyHanoi")
// console.log("tBodyHanoi :", tBody.children)

/*for(let tr = 0; tr < tBody.children.length; tr ++){
    var currentRow = tBody.children[tr].children
    // console.log("tRow :", currentRow)
    for(let td = 0; td < currentRow.length; td ++){
        // console.log("tD :",currentRow[td])
        currentRow[td].addEventListener('click', () => { selectColumn(td) })
    }
}
*/
function selectColumn(columnIndex) {
    currentColumn = tabHanoiGame[columnIndex]
    
    
    if (currentDisque != null) {
        console.log("currentColumn :", currentColumn)
        console.log("currentDisque.colIndex :", currentDisque.colIndex)
        console.log("columnIndex :", columnIndex)
        if(currentDisque.colIndex != columnIndex){
            console.log("currentColumn.disques :", currentColumn.disques)
            for(var d = 0; d < currentColumn.disques.length; d++){
                console.log("disque :", currentColumn.disques[d])
                if(currentColumn.disques[d] === undefined){
                    currentColumn.disques[d] = currentDisque
                    
                    var colRow = document.getElementById("row" + d.toString() + "_"+ currentColumn.name)
                    colRow.appendChild(currentColumn.disques[d].div)
                    
                    break
                }
            }
        }
        currentDisque.div.style.border = ""
    }
    
    
    
    var newDisque = null
    currentColumn.disques.forEach(function (disque) {
        if(disque !== undefined){
            newDisque = disque
        }
    })
    if (newDisque != null && newDisque != currentDisque)
        newDisque.div.style.border = "3px solid black"
    
    if (currentDisque != null && newDisque == currentDisque){
        currentDisque = null
    }
    else{
        currentDisque = newDisque
    }
}


function ActiveValidation() {
    // console.log(columnList[2].getDisques().length === 5)
    if (columnList[2].getDisques().length !== disquesCount) {
        localStorage.setItem("tourHanoiResult", 0);
        window.location.href = './porte.html';
    }
    else {
        localStorage.setItem("tourHanoiResult", 1);
        window.location.href = './porte.html';
    }

}
