var KGDoor = document.getElementById("KGDoor");
var GHDoor = document.getElementById("GHDoor");
var IHDoor = document.getElementById("IHDoor");
var KIDoor = document.getElementById("KIDoor");

var currentDoor = 'null';

function ActivePicture(img) {
    var doorList = [KGDoor, GHDoor, IHDoor, KIDoor];
    doorList.forEach(function (element) {
        element.src = "./image/doorGame/door-close.png";
        element.checked = false;
        if (element.id == img) {
            currentDoor = img;
            element.src = "./image/doorGame/door-open.png";
            element.checked = true;
        }
    })
}

function ActiveValidation() {
    if (currentDoor == 'null') { }
    else {
        if (currentDoor == 'KGDoor') {
            localStorage.setItem("doorKingResult", 1);
            window.location.href = './finish.html';
        }
        else {
            localStorage.setItem("doorKingResult", 0);
            window.location.href = './finish.html';
        }
    }
}