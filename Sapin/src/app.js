/**
 * Création d'un élément du sapin
 * @param parent { HTMLElement }
 * @param width { number }
 * @param isWood { boolean }
 */
function createDiv(parent, width, isWood = false) {
    //On crée l'element HTML
    const div = document.createElement("div")
    // on donne le style à cet element
    div.style.height = "100px"
    div.style.margin = "auto"
    div.style.display = "flex"
    div.id = isWood ? "t_"+width : "b_"+width
    div.style.width = (width * 100).toString() + "px"
    div.style.backgroundColor = isWood ? "#572918" : "#88c450"
    // on l'ajoute à son parent
    parent.appendChild(div)
    
}

/**
 * Creation d'une boule
 * @param parent { HTMLElement }
 * @param size { number }
 */
function createBouleDeNoel(parent, size) {
    //On crée l'element HTML
    const div = document.createElement("div")
    
    // on donne le style à cet element
    div.style.width = size + "px"
    div.style.height = size + "px"
    div.classList.add("boule")
    
    // on ajoute une fonction sur l'evenement de click
    div.addEventListener('click',function () {
        div.style.backgroundColor = "#21a5ec"
    })
    
    // on l'ajoute à son parent
    parent.appendChild(div)
}

/**
 * Fonction pour générer un sapin
 */
function Generation() {
    // si il y à déjà un sapin
    if(document.getElementById("b_1") != null){
        // On boucle sur le conteneur tant qu'il à un enfant
        while (document.getElementById("sapinConteneur").firstChild){
            // on supprime l'enfant du conteneur
            document.getElementById("sapinConteneur").removeChild(document.getElementById("sapinConteneur").firstChild)
        }
    }
    // on récupere le nombre de branches voulue
    const nbBranches = document.getElementById("branchCount").value
    
    // on boucle sur le nombre
    for(let i = 0; i < nbBranches; i++){
        // on ajoute une branche
        createDiv(document.getElementById("sapinConteneur"), (i+1))
        // on boucle sur le numero de la branche pour avoir le nombre de boule
        for(let b = 0; b < (i+1); b++){
            // on ajoute la boule
            createBouleDeNoel(document.getElementById("b_" + (i+1).toString()), 40)
        }
    }
    // on ajoute le tronc
    createDiv(document.getElementById("sapinConteneur"), 1, true)
    // on récupere les boules
    console.log(document.getElementsByClassName("boule"))
}

// on ajoute au boutton un evement
document.getElementById("button").addEventListener('click', Generation)


