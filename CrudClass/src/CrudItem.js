class CrudItem {
    /**
     * @param crudContainerParent { HTMLElement }
     * @param rawData { object }
     * @param buttons { CrudButton[] }
     */
    constructor(crudContainerParent, rawData, buttons= []) {
        this._crudContainerParent = crudContainerParent // CRUD CONTAINER
        
        this._itemContainer = document.createElement("div")
        this._itemContainer.classList.add('CrudItem')
        
        this._crudContainerParent.appendChild(this._itemContainer)
        this.displayInformation(rawData)
    
        
        const buttonDiv = document.createElement('div')
        buttonDiv.classList.add('CrudItemActions')
        buttons.forEach((button) => {
            // button.setCallback(() => {})
            button.setCallback((data) => { this.handleButtonClick(data)})
            button.displayButton(buttonDiv)
        })
        this._itemContainer.appendChild(buttonDiv)
    }
    
    /**
     *
     * @param rawData { object }
     */
    displayInformation(rawData){
        // Object.keys(rawData) => string[]
        Object.keys(rawData).forEach((key) => {
            // Création d'une div
            const informationDiv = document.createElement('div')
            // Création d'une p
            const informationP = document.createElement('p')
            informationP.innerText = rawData[key]
            
            // On met le P dans la Div
            informationDiv.appendChild(informationP)
            // Puis la Div dans l'Item
            this._itemContainer.appendChild(informationDiv)
        })
    }
    
    handleButtonClick(buttonType){
        switch (buttonType) {
            case CRUD_BUTTON_TYPE.DELETE:
                this._crudContainerParent.removeChild(this._itemContainer)
                break
            default:
                console.log("[ "+ buttonType + " ] : not yet implemented")
                break
        }
    }

}
