class Crud{
    /**
     *
     * @param parent { HTMLElement }
     * @param rawData { object[] }
     */
    constructor(parent, rawData) {
        console.warn("[ CRUD ] : new Instance")
        this._parent = parent
        
        this._container = document.createElement('div')
        this._container.classList.add('CrudContainer')
        
        this._parent.appendChild(this._container)
        this.generateItems(rawData)
        
    }
    
    generateItems(_rawData){
        this._itemList = []
        
        _rawData.forEach((data) => {
            this._itemList.push(
                new CrudItem(
                    this._container, // CRUD DIV CONTAINER
                    data, // DATAS DE L'UTILISATEUR
                    [
                        new CrudEditButton(null, 'Editer '), // BOUTONS D'EDTITION
                        new CrudDeleteButton(null, 'Supprimer ') // BOUTONS DE SUPPRESION
                    ]
                )
            )
        })
    }
    
}
