class CrudButton{
    
    /**
     *
     * @param parent { HTMLElement }
     * @param text { string }
     */
    constructor(parent, text ) {
        this._parent = parent;
        
        this._button = document.createElement('button')
        this._button.innerText = text
        this._button.classList.add('btn')
        
        if(this._parent != null)
            this._parent.appendChild(this._button)
    }
    
    /**
     *
     * @param callback { Function }
     */
    setCallback(callback = () => {}){
    }
    
    /**
     *
     * @param divButtonsItemParent { HTMLElement }
     */
    displayButton(divButtonsItemParent){
        this._parent = divButtonsItemParent
        if(this._parent != null)
            this._parent.appendChild(this._button)
    }
}


class CrudEditButton extends CrudButton{
    constructor(parent, text) {
        super(parent, text);
        this._button.classList.add('CrudEdit')
    }
    
    /**
     *
     * @param callback { Function }
     */
    setCallback(callback = () => { console.log('NULL CALLBACK') }){
        this._button.addEventListener('click', ()=> {
            callback(CRUD_BUTTON_TYPE.EDIT)
        })
    }
}



class CrudDeleteButton extends CrudButton{
    constructor(parent, text) {
        super(parent, text);
        this._button.classList.add('CrudDelete')
    }
    /**
     *
     * @param callback { Function }
     */
    setCallback(callback = () => { console.log('NULL CALLBACK') }){
        this._button.addEventListener('click', () => {
            callback(CRUD_BUTTON_TYPE.DELETE)
        })
    }
}


CRUD_BUTTON_TYPE = {
    "ADD" : "add",
    "EDIT" : "edit",
    "DELETE" : "delete",
}
