const button = document.createElement("button")
button.innerText = "Boutton"
button.classList.add("btn")

document.getElementById("main").appendChild(button)

var STATUS = "NORMAL"




button.addEventListener("mouseenter", () => {
    if(STATUS === "NORMAL"){
        button.style.backgroundColor = "yellow"
        STATUS = "HOVER"
    }
})

button.addEventListener("mouseleave", () => {
    if(STATUS === "HOVER"){
        button.style.backgroundColor = "gray"
        STATUS = "NORMAL"
    }
})

button.addEventListener("click", () => {
    if(STATUS === "HOVER"){
        button.style.backgroundColor = "orangered"
        STATUS = "CLICKED"
    }
})




button.addEventListener("focusin", () => {
    if(STATUS === "NORMAL"){
        button.style.backgroundColor = "yellow"
        STATUS = "HOVER"
    }
})

button.addEventListener("focusout", () => {
    if(STATUS === "HOVER"){
        button.style.backgroundColor = "gray"
        STATUS = "NORMAL"
    }
})
