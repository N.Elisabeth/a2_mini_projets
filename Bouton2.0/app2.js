const buttonAEtat = document.createElement("button")
buttonAEtat.setAttribute("id", "buttonAEtat")
buttonAEtat.innerText = "Boutton"
buttonAEtat.classList.add("btn")
document.getElementById("main").appendChild(buttonAEtat)


function changeColor(color) {
    document.getElementById("buttonAEtat").style.backgroundColor = color
}

const states = [
    new Etat("disabled", false)
        .addTransition("OnUnDisabled",new Transition(
            "normal",
            () => {
                // console.log("ToNormal")
                changeColor("gray")
            }
        )),
    new Etat("normal", true)
        .addTransition("OnDisabled", new Transition(
            "disabled",
            () => {
                // console.log("ToDisabled")
                changeColor("black")
            }
        ))
        .addTransition("MouseEnter", new Transition(
            "hover",
            () => {
                // console.log("ToHover")
                changeColor("green")
            }
        )),
    new Etat("hover", false)
        .addTransition( "MouseLeave", new Transition(
            "normal",
            () => {
                // console.log("ToNormal")
                changeColor("gray")
            }
        ))
        .addTransition( "Click", new Transition(
            "clicked",
            () => {
                // console.log("ToClicked")
                changeColor("brown")
            }
        )),
    new Etat("clicked", false)
]
const machineAEtat = new MachineAEtat( "normal", states)
buttonAEtat.addEventListener('mouseenter', () => { machineAEtat.changeState("MouseEnter")})
buttonAEtat.addEventListener('mouseleave', () => { machineAEtat.changeState("MouseLeave")})
buttonAEtat.addEventListener('click', () => { machineAEtat.changeState("Click")})
