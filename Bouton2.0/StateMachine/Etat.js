class Etat {
    
    /**
     * @param name { string }
     * @param initialState { boolean }
     */
    constructor(name, initialState) {
        this.__name = name
        this.__initialState = initialState
        this.__transitions = {}
    }
    
    /**
     * @param transitionName { string }
     * @param transition { Transition}
     * @return Etat
     */
    addTransition(transitionName, transition){
        this.__transitions[transitionName] = transition
        return this
    }
    
    /**
     * @return {string}
     */
    getName(){
        return this.__name;
    }
    
    /**
     * @return {boolean}
     */
    isInitialState(){
        return this.__initialState;
    }
    
    /**
     * @param transitionName { string }
     * @return { Transition | null }
     */
    getTransition(transitionName){
        return this.__transitions[transitionName];
    }
    
}
