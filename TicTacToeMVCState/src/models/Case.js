import Vector2 from "./Vector2.js";
import Sujet from "../Others/Sujet.js";
import Bouton from "./Bouton.js";

export default class Case {
    
    /**
     *
     * @param position { Vector2 }
     */
    constructor(position) {
        // on crée le sujet de la case qui vas permetre de notifier les changement
        this.__sujet = new Sujet()
        
        // bouton
        this.__bouton = new Bouton("")
        
        this.position = position;
        this.content = null;
        
    }
    
    /**
     * @return { Vector2 }
     */
    getPosition(){
        return this.position
    }
    
    /**
     * @return { Bouton }
     */
    getBouton(){
        return this.__bouton
    }
    
    /**
     * @return {Sujet}
     */
    getSujet(){
        return this.__sujet
    }
    
    /**
     * @return {boolean}
     */
    isAlreadySet(){
        return this.content != null;
    }
    
    /**
     * @return {string | null}
     */
    getContent(){
        return this.content;
    }
    
    /**
     * @param player { string }
     */
    setContent(player){
        this.content = player;
        this.__bouton.setName(player === "J1"? "X" : "O")
        this.__bouton.setColor(player === "J1"? "brown" : "blue")
        // on notify que la case à été joué
        this.__sujet.notify("La case vient d'être set")
    }
    
    resetContent(){
        this.content = null;
        this.__bouton.setName("")
        this.__bouton.setColor("gray")
    }
}
