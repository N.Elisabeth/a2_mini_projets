import Sujet from "../Others/Sujet.js";

export default class Bouton {
    
    /**
     * @param name { string }
     * @param color { string}
     */
    constructor(name, color = "grey") {
        // on crée le sujet de la case qui vas permetre de notifier les changement
        this.__sujet = new Sujet()
        
        this.__name = name
        this.__color = color
    }
    
    /**
     * @return {Sujet}
     */
    getSujet(){
        return this.__sujet
    }
    
    /**
     * @param name { string }
     */
    setName(name) { this.__name = name}
    
    /**
     * @return {string}
     */
    getName() { return this.__name }
    
    /**
     * @param color {string}
     */
    setColor(color) {
        this.__color = color
        this.__sujet.notify("")
    }
    
    /**
     * @return {string}
     */
    getColor() { return this.__color}
}
