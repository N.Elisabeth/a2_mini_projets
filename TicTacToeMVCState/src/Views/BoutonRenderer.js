import Observer from "../Others/Observer.js";
import Sujet from "../Others/Sujet.js";

export default class BoutonRenderer {
    
    /**
     * @param boutonModel { Bouton }
     */
    constructor(boutonModel) {
        this.__boutonModel = boutonModel
        
        // on crée l'observer du model
        this.__observer = new Observer("BoutonRenderer")
        this.__boutonModel.getSujet().attach(this.__observer);
        this.__observer.setCallback(() => { this.handleUpdate() })
    
        // on crée le sujet ( vas permettre de notifier au controller quand la case est cliquer )
        this.__boutonSujet = new Sujet()
    
        // on initialise l'HTML
        this.__boutonContainer = null
        this.createHtml()
        this.render()
    }
    
    /**
     * @return { Sujet }
     */
    getSubject(){
        return this.__boutonSujet
    }
    
    /**
     * @return { null | HTMLElement }
     */
    getBoutonContainer(){
        return this.__boutonContainer
    }
    
    createHtml(){
        this.__boutonContainer = document.createElement("div")
        this.__boutonContainer.classList.add("bouton")
        this.__boutonContainer.addEventListener('mouseenter', () => { this.__boutonSujet.notify("MouseEnter") })
        this.__boutonContainer.addEventListener('mouseleave', () => { this.__boutonSujet.notify("MouseLeave") })
        this.__boutonContainer.addEventListener('click', () => { this.__boutonSujet.notify("Click") })
    }
    
    render(){
        if(this.__boutonContainer != null){
            this.__boutonContainer.innerText = this.__boutonModel.getName()
            this.__boutonContainer.style.backgroundColor = this.__boutonModel.getColor()
        }
    }
    
    handleUpdate(){
        this.render()
        // console.log(this.__boutonModel)
    }
}
