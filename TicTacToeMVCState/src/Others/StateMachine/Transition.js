export default class Transition{
    
    /**
     * @param target { string }
     * @param callback { Function }
     */
    constructor(target, callback) {
        this.__target = target
        this.__callback = callback
    }
    
    /**
     * @return { string }
     */
    getTarget(){
        return this.__target
    }
    
    /**
     * @return { Function }
     */
    getCallback(){
        return this.__callback
    }
}
