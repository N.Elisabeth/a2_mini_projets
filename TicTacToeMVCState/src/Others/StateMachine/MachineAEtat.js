export default class MachineAEtat {
    
    /**
     * @param currentStateName { string }
     * @param states { Etat[] }
     */
    constructor(currentStateName, states) {
        this.__startState = currentStateName
        this.__currentStateName = currentStateName
        this.__states = {}
        states.forEach((state) => {
            this.__states[state.getName()] = state
        })
    }
    
    /**
     * @param transitionName { string }
     */
    changeState(transitionName){
        const currentState = this.__states[this.__currentStateName]
        const currentTransition = currentState.getTransition(transitionName)
        // someVar = someTest !== 0 ? "true" : "false"
        if(currentTransition === undefined){
            return this.__currentStateName
        }
        
        currentTransition.getCallback()()
        this.__currentStateName = currentTransition.getTarget()
    }
    
    /**
     * @return { string }
     */
    getCurrentStateName(){
        return this.__currentStateName
    }
    
    resetMachine(){
        this.__currentStateName = this.__startState
    }
}
