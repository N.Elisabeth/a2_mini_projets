export default class Sujet{
    
    constructor() {
        /**
         * @type { Observer[]}
         */
        this.__observers = []
    }
    
    /**
     * Ajouter un observer à la liste
     * @param observer { Observer }
     */
    attach(observer){
        this.__observers.push(observer)
    }
    
    /**
     * Notifier l'ensemble des observer avec un message
     * @param message { * }
     */
    notify(message){
        this.__observers.forEach(function (observer) {
            observer.onObserve(message)
        })
    }
}
