import Observer from "../Others/Observer.js";
import Sujet from "../Others/Sujet.js";
import BoutonController from "./BoutonController.js";

export default class CaseController {
    
    /**
     * @param caseModel { Case }
     * @param caseRenderer { CaseRenderer }
     */
    constructor(caseModel, caseRenderer) {
        // on enregistre le model
        this.__case = caseModel;
        
        this.__boutonController = new BoutonController(this.__case.getBouton(), caseRenderer.getBoutonRenderer().getSubject())
    
        // création de l'observer du boutonController
        this.__observer = new Observer("CaseController["+this.__case.getPosition().x+";"+this.__case.getPosition().y+"]")
        this.__boutonController.getBoutonControllerSujet().attach(this.__observer)
        this.__observer.setCallback( () => { this.caseClicked() })
        
        // on crée un sujet pour pouvoir prévenir la Grille Controller quand notre case est jouer
        this.__caseControllerSubject = new Sujet()
    }
    
    getControllerSubject(){
        return this.__caseControllerSubject
    }
    
    caseClicked(){
        if(this.__case.isAlreadySet() === false)
            this.__caseControllerSubject.notify(this.__case.position)
            
    }
    
    reset(){
        this.__boutonController.reset()
    }
}
