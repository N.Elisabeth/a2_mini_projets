import Observer from "../Others/Observer.js";
import MachineAEtat from "../Others/StateMachine/MachineAEtat.js";
import Etat from "../Others/StateMachine/Etat.js";
import Transition from "../Others/StateMachine/Transition.js";
import Sujet from "../Others/Sujet.js";

export default class BoutonController {
    
    
    /**
     * @param bouton { Bouton }
     * @param boutonSujet { Sujet }
     */
    constructor(bouton, boutonSujet) {
        this.__bouton = bouton
        this.__boutonSujet = boutonSujet
    
        this.__machineAEtat = new MachineAEtat("normal", [
            new Etat("disabled", false)
                .addTransition("OnUnDisabled",new Transition(
                    "normal",
                    () => {
                        // console.log("ToNormal")
                        this.__bouton.setColor("gray")
                    }
                )),
            new Etat("normal", true)
                .addTransition("OnDisabled", new Transition(
                    "disabled",
                    () => {
                        // console.log("ToDisabled")
                        this.__bouton.setColor("black")
                    }
                ))
                .addTransition("MouseEnter", new Transition(
                    "hover",
                    () => {
                       // console.log("ToHover")
                        this.__bouton.setColor("green")
                    }
                )),
            new Etat("hover", false)
                .addTransition( "MouseLeave", new Transition(
                    "normal",
                    () => {
                        // console.log("ToNormal")
                        this.__bouton.setColor("gray")
                    }
                ))
                .addTransition( "Click", new Transition(
                    "clicked",
                    () => {
                        //console.log("ToClicked")
                        this.__boutonControllerSubject.notify("")
                    }
                )),
            new Etat("clicked", false)
        ])
        
        // création de l'observer du click sur la div
        this.__observer = new Observer("CaseController["+this.__bouton.getName()+"]")
        boutonSujet.attach(this.__observer)
        this.__observer.setCallback( (data) => { this.boutonClicked(data) })
    
        this.__boutonControllerSubject = new Sujet()
    }
    
    /**
     * @return { Sujet }
     */
    getBoutonControllerSujet(){
        return this.__boutonControllerSubject
    }
    
    boutonClicked(data){
        this.__machineAEtat.changeState(data)
    }
    
    reset(){
        this.__machineAEtat.resetMachine()
    }
}
