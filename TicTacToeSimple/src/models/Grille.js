import Case from "./Case.js";
import Vector2 from "./Vector2.js";

export default class Grille {
    
    parent = null // Element HTML
    gridDiv = null // Element HTML
    cases = [] // Tablaux
    
    /*
    * cases = [ case_1, ... ]
    * cases = [
    *   [ case_1, ...]
    *   [ case_1, ...]
    *   [ case_1, ...]
    * ]
    * */
    
    constructor(parent, gridSize) {
        this.parent = parent;
        
        this.gridSize = gridSize;
        
        this.gridDiv = document.createElement('div');
        this.gridDiv.setAttribute("id", "grid");
        this.parent.appendChild(this.gridDiv);
        
        
        this.currentPlayer = "J1";
        this.cases = [];
        
        // Boucles d'initialisation de la grille
        for (let x = 0; x < gridSize; x++){
            for (let y = 0; y < gridSize; y++){
                // ici on instancie une nouvelle Case
                const newCase = new Case( new Vector2( x, y), this.gridDiv);
                
                // ici on attribue une fonction à l'évenement 'click' de la div de la case
                newCase.getHtmlElement().addEventListener('click', () => {
                    // on call la function qui va gérer le tout de jeu en lui envoyant la position de la case
                    this.playerPlay(newCase);
                });
                
                // ici on ajoute la case au tableau de la grille pour ne pas la perdre
                this.cases.push(newCase);
            }
        }
    }
    
    
    getCase(position){
        return this.cases[position.x * this.gridSize + position.y];
    }
    
    
    // Functions
    playerPlay(currentCase){
        console.log(currentCase);
        
        if(currentCase.isAlreadySet()){
            return;
        }
        
        currentCase.setContent(this.currentPlayer);
        
        this.currentPlayer = this.currentPlayer === "J1" ? "J2" : "J1"
        
        const winner = this.testWin()
        if(winner != null){
            alert("Le joueur " + winner + "a gagné" )
            const button = document.getElementById("ResetButton");
            button.hidden = false;
        }
    }
    
    
    testWin(){
        // COLONNES
        if(this.cases[0].getContent() === this.cases[1].getContent() && this.cases[1].getContent() === this.cases[2].getContent()){
            return this.cases[0].getContent();
        }
        if(this.cases[3].getContent() === this.cases[4].getContent() && this.cases[4].getContent() === this.cases[5].getContent()){
            return this.cases[3].getContent();
        }
        if(this.cases[6].getContent() === this.cases[7].getContent() && this.cases[7].getContent() === this.cases[8].getContent()){
            return this.cases[6].getContent();
        }
        
        // LIGNES
        if(this.cases[0].getContent() === this.cases[3].getContent() && this.cases[3].getContent() === this.cases[6].getContent()){
            return this.cases[0].getContent();
        }
        if(this.cases[1].getContent() === this.cases[4].getContent() && this.cases[4].getContent() === this.cases[7].getContent()){
            return this.cases[1].getContent();
        }
        if(this.cases[2].getContent() === this.cases[5].getContent() && this.cases[5].getContent() === this.cases[8].getContent()){
            return this.cases[2].getContent();
        }
        
        // Diagonales
        if(this.cases[0].getContent() === this.cases[4].getContent() && this.cases[4].getContent() === this.cases[8].getContent()){
            return this.cases[0].getContent();
        }
        if(this.cases[2].getContent() === this.cases[4].getContent() && this.cases[4].getContent() === this.cases[6].getContent()){
            return this.cases[2].getContent();
        }
        
        return null;
        
    }
    
    
    reset(){
        this.cases.forEach(function (currentCase) {
            currentCase.resetContent();
        })
    }
    
}
