import Vector2 from "./Vector2.js";

export default class Case {
    
    /**
     *
     * @param position { Vector2 }
     */
    constructor(position, parent) {
        this.position = position;
        this.parent = parent;
        this.content = null;
        
        this.caseDiv = document.createElement('div')
        this.caseDiv.classList.add("cases")
        
        // ligne de depart 1 / colomn de depart 1 / ligne de fin 2 / colomn de fin 2
        this.caseDiv.style.gridArea = (position.y + 1) + "/" + (position.x + 1) + "/" + (position.y + 2) + "/" + (position.x + 2);
        this.parent.appendChild(this.caseDiv)
    }
    
    getHtmlElement(){
        return this.caseDiv;
    }
    
    
    isAlreadySet(position){
        return this.content != null;
    }
    
    getContent(){
        return this.content;
    }
    
    setContent(player){
        this.content = player;
        const paragraph = document.createElement('h1')
        paragraph.innerText = player === "J1" ? "X" : "O";
        this.caseDiv.appendChild(paragraph)
    }
    
    resetContent(){
        this.content = null;
        if(this.caseDiv.firstChild){
            this.caseDiv.removeChild(this.caseDiv.firstChild);
        }
    }
}
