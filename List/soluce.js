const personnes = [
    {
        "firstName": "Luffy",
        "lastName": "Monkey",
        "isAD": true,
        "hakis": ["roi","observation", "renforcement"],
        "pirateName": "Luffy au chapeaux de paille",
        "Affiliation": ["pire Generation", "Mugiwara"]
    },
    {
        "firstName": "Zoro",
        "lastName": "Roronoa",
        "isAD": false,
        "hakis": ["roi", "renforcement"],
        "pirateName": "Roronoa Zoro le chasseur de prime",
        "Affiliation": ["pire Generation", "Mugiwara"]
    },
    {
        "firstName": "Water Law",
        "lastName": "Trafalger",
        "isAD": true,
        "hakis": [],
        "pirateName": "Le Chirurgien de la Mort",
        "Affiliation": ["pire Generation", "L'equipage du hearts"]
    },
    {
        "firstName": "Doflamingo",
        "lastName": "Don quichotte",
        "isAD": false,
        "hakis": ["roi", "renforcement"],
        "pirateName": "Joker",
        "Affiliation": ["sept corsaire", "Equipage de Don quichotte Doflamingo"]
    },
    {
        "firstName": "Moria",
        "lastName": "Gecko",
        "isAD": false,
        "hakis": [],
        "pirateName": "Gecko",
        "Affiliation": ["sept corsaire", "Equipage de Don quichotte Doflamingo"]
    },
    {
        "firstName": "Kaido",
        "lastName": "",
        "isAD": false,
        "hakis": ["roi", "renforcement"],
        "pirateName": "Kaido",
        "Affiliation": ["Empereur", "Equipage aux cents bête"]
    },
    {
        "firstName": "Katakuri",
        "lastName": "Geko",
        "isAD": false,
        "hakis": ["roi", "observation", "renforcement"],
        "pirateName": "Katakuri",
        "Affiliation": ["Equipage de Big Mom"]
    },
]
console.log(personnes)

let result_1 = personnes.filter(function (pirate) {
    return pirate.isAD
})
console.log("result 1:", result_1)


let result_two = []
for(let p = 0; p < personnes.length; p ++){
    if(personnes[p].isAD){
        result_two.push(personnes[p].pirateName)
    }
}
console.log("result 2:", result_two)

let result_3 = personnes.filter(function (pirate) {
    return pirate.isAD
}).map(function (_pirate) {
    return _pirate.pirateName
})
console.log("result 2:", result_3)


/**
 *
 * @param haki {string}
 */
const find_how_many_have_the_haki = (haki) => {
    let counter = 0;
    for(let i = 0; i < personnes.length; i ++){
        if(personnes[i].hakis.indexOf(haki) > -1){
            counter ++
        }
    }
    console.log(counter, "personnes on le haki de", haki)
    return counter
}

const result = {}
const hakiList = ["roi", "renforcement", "observation", "couleur"]
hakiList.forEach(function (hakiName) {
    result[hakiName] = find_how_many_have_the_haki(hakiName)
})
console.log("result 1:", result)
