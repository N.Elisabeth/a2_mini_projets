const personnes = [
    {
        "firstName": "Luffy",
        "lastName": "Monkey",
        "isAD": true,
        "hakis": ["roi","observation", "renforcement"],
        "pirateName": "Luffy au chapeaux de paille",
        "Affiliation": ["pire Generation", "Mugiwara"]
    },
    {
        "firstName": "Zoro",
        "lastName": "Roronoa",
        "isAD": false,
        "hakis": ["roi", "renforcement"],
        "pirateName": "Roronoa Zoro le chasseur de prime",
        "Affiliation": ["pire Generation", "Mugiwara"]
    },
    {
        "firstName": "Water Law",
        "lastName": "Trafalger",
        "isAD": true,
        "hakis": [],
        "pirateName": "Le Chirurgien de la Mort",
        "Affiliation": ["pire Generation", "L'equipage du hearts"]
    },
    {
        "firstName": "Doflamingo",
        "lastName": "Don quichotte",
        "isAD": false,
        "hakis": ["roi", "renforcement"],
        "pirateName": "Joker",
        "Affiliation": ["sept corsaire", "Equipage de Don quichotte Doflamingo"]
    },
    {
        "firstName": "Moria",
        "lastName": "Gecko",
        "isAD": false,
        "hakis": [],
        "pirateName": "Gecko",
        "Affiliation": ["sept corsaire", "Equipage de Don quichotte Doflamingo"]
    },
    {
        "firstName": "Kaido",
        "lastName": "",
        "isAD": false,
        "hakis": ["roi", "renforcement"],
        "pirateName": "Kaido",
        "Affiliation": ["Empereur", "Equipage aux cents bête"]
    },
    {
        "firstName": "Katakuri",
        "lastName": "Geko",
        "isAD": false,
        "hakis": ["roi", "observation", "renforcement"],
        "pirateName": "Katakuri",
        "Affiliation": ["Equipage de Big Mom"]
    },
]
//console.log(personnes)

let result = personnes.filter(function (character) {
    return character.isAD
})
console.log("result 1:", result)


result = personnes.filter(function (character) {
    return character.isAD
}).map(function (pirate) {
    return pirate["pirateName"]
})
console.log("result 2:", result)


researchedHaki = ["roi"]
result = personnes.filter(function (character) {
    let val = true
    researchedHaki.forEach(function (hakiName) {
        if (character["hakis"].includes(hakiName) === false){
            val = false;
        }
    })
    return val
}).map(function (pirate) {
    return pirate["pirateName"]
})
console.log("result 3:", result)


researchedHaki = ["roi", "renforcement", "observation"]
hakiCount = {}
researchedHaki.forEach(function (hakiName) {
    hakiCount[hakiName] = personnes.filter(function (pirate) {
        return pirate["hakis"].includes(hakiName)
    }).length
})
console.log("result 4:", hakiCount)
