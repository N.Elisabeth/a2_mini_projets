var validation = false;
var p = "";
//variable pour l'adresse de la page suivante
var a = 'index';
var gateau = '';
var choix = '';
const extension = '.png';
const retourMessage = document.getElementById("message");

function error(texte){
    //vérif de l'extention
    var verifExtension = texte.indexOf(extension);
    //vérif de l'espace
    var espace = texte.lastIndexOf(" ");
    //vérif crochet ouvrant
    var crochet1 = texte.lastIndexOf('<');
    //vérif crochet ferment
    var crochet2 = texte.indexOf('>');
    //vérif deux deux-points
    var deuxPoint = texte.indexOf(':');
    //vérif point-virgule
    var pointVirgule = texte.indexOf(';');
    
    validation = false;
    retourMessage.style.color = "#d30202"
    document.getElementById("suivant").classList.remove("vald")
    if (localStorage.getItem("deux") == null){
        if (verifExtension<0){
            retourMessage.textContent = 'Mauvaise extention !'
        }
        else if (crochet1>1 && crochet2==0){
            retourMessage.textContent = "Les deux crochets ne sont pas dans le bon sens."
        }
        else if (crochet1>1 || crochet2==0){
            retourMessage.textContent = "L'un des deux crochet n'est pas dans le bon sens."
        }
        else if (crochet1<0 && crochet2<0){
            retourMessage.textContent = "Il manque les crochets."
        }
        else if (crochet1<0 || crochet2<0){
            retourMessage.textContent = "Il manque un des deux crochets."
        }
    }

    else if (localStorage.getItem("deux") != null){
        if (deuxPoint<0){
            retourMessage.textContent = 'Il manque les deux-points avant le parfum'
        }
        else if (pointVirgule<0){
            retourMessage.textContent = "Il manque le point-virgule à la fin"
        }
    }

    else {
        retourMessage.textContent = "Il y a une erreur dans le code."
    }
}

function change() {
    if (validation == true) {
        localStorage.setItem(a, gateau);
        localStorage.setItem("choix"+a, localStorage.getItem("choix"+p)+choix);
        document.location.href = a+".html";
    }
    else {
        alert("Tu dois valider avant de faire suivant");
    }
}

function verifCode() {
    var texteOriginaux = [
        {"value":'<img src="assets/img/NomDuGateau" alt="Gâteau">'},
        {"value":'background-color: Couleur;'},
        {"value":'color: Couleur;'},
    ]

    var reponses = [
        {
            "value":'<img src="assets/img/gateau1.png" alt="Gâteau">',
            "img":"assets/img/gateau1.png",
            "p":"index",
            "a":"deux", 
            "choix":"1",
            "message": "Tu as choisi le premier gateau !",
        },
        {
            "value":'<img src="assets/img/gateau2.png" alt="Gâteau">',
            "img":"assets/img/gateau2.png",
            "p":"index",
            "a":"deux",
            "choix":"2",
            "message": "Tu as choisi le deuxieme gateau !",
        },
        {
            "value":'<img src="assets/img/gateau3.png" alt="Gâteau">',
            "img":"assets/img/gateau3.png",
            "p":"index",
            "a":"deux",
            "choix":"3",
            "message": "Tu as choisi le troisième gateau !",
        },
        {
            "value":'<img src="assets/img/gateau4.png" alt="Gâteau">',
            "img":"assets/img/gateau4.png",
            "p":"index",
            "a":"deux",
            "choix":"4",
            "message": "Tu as choisi le quatrième gateau !",
        },


        {
            "value":'background-color: chocolat;',
            "img":"assets/img/gateau1.png",
            "p":"deux",
            "a":"trois",
            "choix":"Ch",
            "message": "Tu as choisi la saveur chocolat !",
        },
        {
            "value":'background-color: pistache;',
            "img":"assets/img/gateau2.png",
            "p":"deux",
            "a":"trois",
            "choix":"Pi",
            "message": "Tu as choisi la saveur pistache !",
        },
        {
            "value":'background-color: fraise;',
            "img":"assets/img/gateau3.png",
            "p":"deux",
            "a":"trois",
            "choix":"Fr",
            "message": "Tu as choisi la saveur fraise !",
        },
        {
            "value":'background-color: citron;',
            "img":"assets/img/gateau4.png",
            "p":"deux",
            "a":"trois",
            "choix":"Ci",
            "message": "Tu as choisi la saveur citron !",
        },

        
        {
            "value":'color: chocolat;',
            "img":"assets/img/gateau1.png",
            "p":"trois",
            "a":"quatre",
            "choix":"Ch",
            "message": "Tu as choisi le nappage saveur chocolat !",
        },
        {
            "value":'color: pistache;',
            "img":"assets/img/gateau2.png",
            "p":"trois",
            "a":"quatre",
            "choix":"Pi",
            "message": "Tu as choisi le nappage saveur pistache !",
        },
        {
            "value":'color: fraise;',
            "img":"assets/img/gateau3.png",
            "p":"trois",
            "a":"quatre",
            "choix":"Fr",
            "message": "Tu as choisi le nappage saveur fraise !",
        },
        {
            "value":'color: citron;',
            "img":"assets/img/gateau4.png",
            "p":"trois",
            "a":"quatre",
            "choix":"Ci",
            "message": "Tu as choisi le nappage saveur citron !",
        }
    ]

    var texte = document.getElementById("code").value;

    const original = texteOriginaux.find(function(reponse){
        return texte == reponse.value
    })

    const element = reponses.find(function(reponse){
        return texte == reponse.value
    })

    if (original != null){
        document.getElementById("suivant").classList.remove("vald")
        retourMessage.style.color = "#d30202"
        retourMessage.textContent = "Copier/coller ne suffit pas, il faut changer le texte en bleu."
        validation = false;
    }

    else if(element != null){
        choix = element.choix;
        p = element.p;
        a = element.a;

        console.log("assets/img/gateau"+localStorage.getItem("choix"+element.p)+choix+".png");

        image.src = "assets/img/gateau"+localStorage.getItem("choix"+element.p)+choix+".png";
        gateau = image.src;
        
        retourMessage.style.color = "#0b8600"
        retourMessage.textContent = element.message;
        
        document.getElementById("suivant").classList.add("vald")
        validation = true;
    }

    else{
        error(texte)
    }
}