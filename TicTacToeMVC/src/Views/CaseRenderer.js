import Observer from "../Others/Observer.js";
import Sujet from "../Others/Sujet.js";

export default class CaseRenderer {
    
    /**
     * @param caseModel { Case }
     */
    constructor(caseModel) {
        // on enregistre le model
        this.__case = caseModel
    
        // on crée l'observer du model
        this.__observer = new Observer("CaseRenderer["+this.__case.getPosition().x+";"+this.__case.getPosition().y+"]")
        this.__case.getSujet().attach(this.__observer);
        this.__observer.setCallback(() => { this.handleUpdate() })
        
        // on crée le sujet ( vas permettre de notifier au controller quand la case est cliquer )
        this.__boutonSujet = new Sujet()
        
        // on initialise l'HTML
        this.__caseContainer = null
        this.createHtml()
    }
    
    /**
     * @return { Sujet }
     */
    getSubject(){
        return this.__boutonSujet
    }
    
    /**
     * @return { null | HTMLElement }
     */
    getCaseContainer(){
        return this.__caseContainer
    }
    
    createHtml(){
        this.__caseContainer = document.createElement("div")
        this.__caseContainer.classList.add("cases")
        this.__caseContainer.addEventListener('click', () => { this.__boutonSujet.notify("Clicked") })
    }
    
    render(){
        if(this.__caseContainer != null){
            if(this.__case.isAlreadySet() !== false){
                this.__caseContainer.innerText = this.__case.getContent() === "J1" ? "X" : "O"
            }else{
                this.__caseContainer.innerText = ""
            }
        }
    }
    
    handleUpdate(){
        this.__caseContainer.innerText = this.__case.getContent() === "J1" ? "X" : "O"
        console.log(this.__case)
    }
}
