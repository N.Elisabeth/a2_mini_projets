import Observer from "../Others/Observer.js";
import CaseRenderer from "./CaseRenderer.js";
import Vector2 from "../models/Vector2.js";
import Sujet from "../Others/Sujet.js";

export default class GrilleRenderer {
    
    /**
     * @param grille { Grille }
     */
    constructor(grille) {
        // on sauvegarde le model
        this.__grille = grille
        
        // on crée l'observer du model ( la grille )
        this.__observer = new Observer("GrilleView")
        this.__grille.getSujet().attach(this.__observer);
        this.__observer.setCallback((data) => { this.handleUpdate(data) })
    
        // création du sujet pour le boutton
        this.__boutonSujet = new Sujet()
        
        /**
         * @type { CaseRenderer[] }
         */
        this.__caseRenderers = []
        // On crée les caseRenderer
        for (let x = 0; x < this.__grille.getGrilleSize(); x++){
            for (let y = 0; y < this.__grille.getGrilleSize(); y++){
                this.__caseRenderers.push(
                    new CaseRenderer(
                        this.__grille.getCase(new Vector2(x, y))
                    )
                )
            }
        }
        
        // on initialise les variables d'HTML
        this.__gridContainer = null
        this.__divWinner = null
        this.createHtml()
    }
    
    /**
     * @return { Sujet }
     */
    getBoutonSujet(){
        return this.__boutonSujet
    }
    
    createHtml(){
        this.__gridContainer = document.createElement("div")
        this.__gridContainer.setAttribute("id", "grid");
        document.getElementById("ApplicationParent").appendChild(this.__gridContainer)
    
        this.__caseRenderers.forEach((caseRenderer) =>  {
            this.__gridContainer.appendChild(caseRenderer.getCaseContainer())
        })
    }
    
    render(){
        if(this.__gridContainer != null){
            this.__caseRenderers.forEach((caseRenderer) =>  {
                caseRenderer.render()
            })
            
            if(this.__grille.hasWinner() === false){
                document.getElementById("ApplicationParent").removeChild(this.__divWinner)
                this.__divWinner = null
            }
        }
    }
    
    /**
     * @param position { Vector2 }
     * @return { CaseRenderer }
     */
    getCaseRenderer(position){
        return this.__caseRenderers[position.x * this.__grille.getGrilleSize() + position.y];
    }
    
    /**
     * Fonction qui s'active quand un joueur à gagner
     * @param data
     */
    handleUpdate(data){
        this.__divWinner = document.createElement("div")
        
        const playerTitle = document.createElement("h2")
        playerTitle.innerText =  "Bravo " + data
        
        const button = document.createElement("button")
        button.innerText = "Recommencer"
        button.addEventListener("click", () => { this.__boutonSujet.notify() })
    
    
        this.__divWinner.appendChild(playerTitle)
        this.__divWinner.appendChild(button)
        this.__divWinner.setAttribute("id", "divWinner")
        
        document.getElementById("ApplicationParent").appendChild(this.__divWinner)
        
    }
    
}
