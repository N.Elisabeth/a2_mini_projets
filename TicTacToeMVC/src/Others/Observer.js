export default class Observer {
    
    /**
     * @param name { string }
     */
    constructor(name) {
        // INITIALISATION
        this.__name = name
        this.__callback = null;
    }
    
    /**
     * Mise en place d'une callback qui sera appeller quand l'observer est notifier
     * @param callback { Function }
     */
    setCallback(callback){
        this.__callback = callback
    }
    
    /**
     * Reception de notification
     * @param message { * }
     */
    onObserve(data){
        console.log(this.__name + " : " + data)
        // si une callback à été mise on active la callback
        if(this.__callback != null)
            this.__callback(data)
    }
}
