import Grille from "../models/Grille.js";
import CaseController from "./CaseController.js";
import Vector2 from "../models/Vector2.js";
import GrilleRenderer from "../Views/GrilleRenderer.js";
import Observer from "../Others/Observer.js";

export default class GrilleController {
    
    constructor(size) {
        // On initialise la grille ( Le model )
        this.__grille = new Grille(size)
    
        // On initialise la GrilleRenderer ( La vue )
        this.__grilleView = new GrilleRenderer(this.__grille)
    
        // création d'un premier observer [__resetObserver] pour le bouton de fin
        this.__resetObserver = new Observer("GrilleController : reset")
        // on attache l'observer [__resetObserver] au sujet
        this.__grilleView.getBoutonSujet().attach(this.__resetObserver)
        // on set la callback
        this.__resetObserver.setCallback(() => { this.reset() })
        
        // création d'un second observer [__grilleControlerObserver] pour la partie ( quand on clique sur une case )
        this.__grilleControlerObserver = new Observer("GrilleController : grille")
        // on set la callback
        this.__grilleControlerObserver.setCallback((position) => { this.playTurn(position) })
        
        // on crée notre tableaux de caseController
        this.__caseControllers = []
        // on boucle sur la grille
        for (let x = 0; x < this.__grille.getGrilleSize(); x++){
            for (let y = 0; y < this.__grille.getGrilleSize(); y++){
                // on push un nouveau CaseController dans le tableau
                this.__caseControllers.push(
                    new CaseController(
                        // ici on recupére la caseModel
                        this.__grille.getCase( new Vector2(x, y)),
                        // on récupére le sujet qui est en faite l'évenement click sur la div de la case
                        this.__grilleView.getCaseRenderer(new Vector2(x, y)).getSubject()
                    )
                )
                // on attache l'observer [__grilleControlerObserver] au sujet
                this.__caseControllers[this.__caseControllers.length - 1].getControllerSubject().attach(this.__grilleControlerObserver)
            }
        }
    }
    
    /**
     * @return { Grille }
     */
    getGrilleModel(){
        return this.__grille
    }
    
    playTurn(position){
        // on verifie qui à pas de gagnanr
        if(this.__grille.hasWinner() !== true){
            // on demande à la grille de jouer
            this.__grille.setCase(position)
            // console.log(position)
        }
    }
    
    reset(){
        this.__grille.reset()
        // on relance le rendu pour actualiser l'affichage
        this.__grilleView.render()
    }
}
