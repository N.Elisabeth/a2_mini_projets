import Observer from "../Others/Observer.js";
import Sujet from "../Others/Sujet.js";

export default class CaseController {
    
    /**
     * @param caseModel { Case }
     * @param boutonSujet { Sujet }
     */
    constructor(caseModel, boutonSujet) {
        // on enregistre le model
        this.__case = caseModel;
        
        // création de l'observer du click sur la div
        this.__observer = new Observer("CaseController["+this.__case.getPosition().x+";"+this.__case.getPosition().y+"]")
        boutonSujet.attach(this.__observer)
        this.__observer.setCallback( () => { this.caseClicked() })
        
        // on crée un sujet pour pouvoir prévenir la Grille Controller quand notre case est jouer
        this.__caseControllerSubject = new Sujet()
    }
    
    getControllerSubject(){
        return this.__caseControllerSubject
    }
    
    caseClicked(){
        if(this.__case.isAlreadySet() === false)
            this.__caseControllerSubject.notify(this.__case.position)
            
    }
}
