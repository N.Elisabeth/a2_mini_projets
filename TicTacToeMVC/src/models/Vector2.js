/**
 * Ceci est une classe vectoriel
 * @class
 */
export default class Vector2 {
    
    /**
     * Create Vector2
     * @param x {number}
     * @param y {number}
     */
    constructor(x, y) {
        this.x = x;
        this.y = y;
    }
    
    /**
     * Addition de deux vector2
     * @param vector { Vector2 }
     */
    add(vector){
        this.x += vector.x;
        this.y += vector.y;
    }
    
    /**
     * Addition de deux vector2
     * @param vector { Vector2 }
     */
    remove(vector){
        this.x -= vector.x;
        this.y -= vector.y;
    }
    
    
}
