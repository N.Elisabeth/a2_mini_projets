import Case from "./Case.js";
import Vector2 from "./Vector2.js";
import Sujet from "../Others/Sujet.js";

export default class Grille {
    
    constructor(gridSize) {
        
        // INITIALISATION
        this.__grilleSujet = new Sujet()
        this.__gridSize = gridSize;
        this.__winner = null
        this.currentPlayer = "J1";
        
        
        this.cases = [];
        // Boucles pour crée les CaseModel
        for (let x = 0; x < gridSize; x++){
            for (let y = 0; y < gridSize; y++){
                // ici on instancie une nouvelle Case
                const newCase = new Case(new Vector2( x, y));
                this.cases.push(newCase);
            }
        }
    }
    
    /**
     * @return {boolean}
     */
    hasWinner(){
        return this.__winner != null
    }
    
    /**
     * @return { Sujet }
     */
    getSujet(){
        return this.__grilleSujet
    }
    
    /**
     * @return { number }
     */
    getGrilleSize(){
        return this.__gridSize
    }
    
    /**
     * @param position { Vector2 }
     * @return { Case }
     */
    getCase(position){
        return this.cases[position.x * this.__gridSize + position.y];
    }
    
    
    /**
     * @param position { Vector2 }
     */
    setCase(position){
        // on set le content de la case avec le joueur actuelle
        this.getCase(position).setContent(this.currentPlayer);
        
        // on change le joueuur actuelle
        this.currentPlayer = this.currentPlayer === "J1" ? "J2" : "J1"
        
        // on regarde si il y à un gagnant
        this.__winner = this.testWin()
        if(this.__winner != null){
            // si oui on notify le rendu
            this.__grilleSujet.notify(this.__winner)
        }
    }
    
    /**
     * @return {null | string}
     */
    testWin(){
        // COLONNES
        if(this.cases[0].getContent() === this.cases[1].getContent() && this.cases[1].getContent() === this.cases[2].getContent()){
            return this.cases[0].getContent();
        }
        if(this.cases[3].getContent() === this.cases[4].getContent() && this.cases[4].getContent() === this.cases[5].getContent()){
            return this.cases[3].getContent();
        }
        if(this.cases[6].getContent() === this.cases[7].getContent() && this.cases[7].getContent() === this.cases[8].getContent()){
            return this.cases[6].getContent();
        }
        
        // LIGNES
        if(this.cases[0].getContent() === this.cases[3].getContent() && this.cases[3].getContent() === this.cases[6].getContent()){
            return this.cases[0].getContent();
        }
        if(this.cases[1].getContent() === this.cases[4].getContent() && this.cases[4].getContent() === this.cases[7].getContent()){
            return this.cases[1].getContent();
        }
        if(this.cases[2].getContent() === this.cases[5].getContent() && this.cases[5].getContent() === this.cases[8].getContent()){
            return this.cases[2].getContent();
        }
        
        // Diagonales
        if(this.cases[0].getContent() === this.cases[4].getContent() && this.cases[4].getContent() === this.cases[8].getContent()){
            return this.cases[0].getContent();
        }
        if(this.cases[2].getContent() === this.cases[4].getContent() && this.cases[4].getContent() === this.cases[6].getContent()){
            return this.cases[2].getContent();
        }
        return null;
    }
    
    
    reset(){
        // la pour toute les case on reset
        this.cases.forEach(function (currentCase) {
            currentCase.resetContent();
        })
        // on remet à zero les valeurs de jeu
        this.__winner = null;
        this.currentPlayer = "J1"
    }
    
}
